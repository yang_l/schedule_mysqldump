package com.example.mysqldumptest.constant;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author YangLi
 * @Date 2024/1/24 10:15
 * @注释
 */
public class MysqldumpConstant {

    public static final ThreadPoolExecutor EXECUTOR = new ThreadPoolExecutor(5, 10, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<>());

    public static final String logFilePath = "D:\\decore\\web\\codeBackup\\" + "log.txt";

    public static final String mysqlFilePath = "D:\\decore\\web\\codeBackup\\";
}
