package com.example.mysqldumptest.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.example.mysqldumptest.constant.MysqldumpConstant;

/**
 * @Author YangLi
 * @Date 2024/1/19 14:57
 * @注释
 */
@Component
@Log4j2
public class TaskFileScheduleService {

    /**
     * @Async 这个注解表示每一个任务都是异步执行 线程池会使用新的线程来执行这个任务
     * public void task1()  这个方法不能有参数     这个方法也不能又返回值
     * 这个方法中  也不能使用  通过注入方式   注入的类   比如注入   xxx.mapper  等
     */
    @Async
    @Scheduled(cron = "0 0 6 * * 1-5")
    public void task1() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss");
        String format = now.format(formatter);

        String mcntChmod = "mysqldump --skip-column-statistics -h192.168.1.114 -P3306 -uroot -pvecentek@mcnt wctt_mcnt";
        String mcntFileName = "\\mcnt\\wctt_mcnt_" + format + ".sql";

        String btChmod = "mysqldump --skip-column-statistics -h192.168.1.100 -P3306 -uroot -pvecentek@bluetooth wctt_bt";
        String btFileName = "\\bt\\wctt_bt_" + format + ".sql";

        String comalsChmod = "mysqldump --skip-column-statistics -h192.168.1.113 -P3306 -uroot -pvecentek@comals wctt_comals";
        String comalsFileName = "\\comals\\wctt_comals_" + format + ".sql";

        String v2xChmod = "mysqldump --skip-column-statistics -h192.168.1.105 -P3306 -uroot -pvecentek@v2x wctt_v2x";
        String v2xFileName = "\\v2x\\wctt_v2x_" + format + ".sql";

        String platformChmod = "mysqldump --skip-column-statistics -h192.168.1.150 -P3306 -uroot -pvecentek@istms wctt_testplatform";
        String platformFileName = "\\platform\\wctt_testplatform_" + format + ".sql";

        String wifiChmod = "mysqldump --skip-column-statistics -h192.168.1.101 -P3306 -uroot -p123456   wifidb_dev";
        String wifiFileName = "\\wifi\\wifidb_dev_" + format + ".sql";

        String rfChmod = "mysqldump --skip-column-statistics -h192.168.1.104 -P3306 -uroot -pvecentek@rfk wctt_rf";
        String rfFileName = "\\rf\\wctt_rf_" + format + ".sql";

        String gnssChmod = "mysqldump --skip-column-statistics -h192.168.1.102 -P3306 -uroot -pvecentek@gnss   wctt_gnss";
        String gnssFileName = "\\gnss\\wctt_gnss_" + format + ".sql";

        String vstChmod = "mysqldump --skip-column-statistics -h192.168.1.130 -P3306 -uroot -pVst@123456   vst";
        String vstFileName = "\\vst\\vst_" + format + ".sql";

        String binaryChmod = "mysqldump --skip-column-statistics -h192.168.1.131 -P3306 -uroot -pvecentek@binary wctt_binary_dev";
        String binaryFileName = "\\binary\\wctt_binary_dev_" + format + ".sql";

        String dstChmod = "mysqldump --skip-column-statistics -h192.168.1.132 -P3306 -uroot -pvecentek@dstdb  data_security_pub";
        String dstFileName = "\\dst\\data_security_pub_" + format + ".sql";

        String appChmod = "mysqldump --skip-column-statistics -h192.168.1.133 -P3306 -uroot -pvecentek@app wctt_app";
        String appFileName = "\\app\\wctt_app_" + format + ".sql";

        String vcanChmod = "mysqldump --skip-column-statistics -h192.168.1.110 -P3306 -uroot -pvcan@vecentek wctt_vcan";
        String vcanFileName = "\\vcan\\wctt_vcan_" + format + ".sql";

        String vetChmod = "mysqldump --skip-column-statistics -h192.168.1.111 -P3306 -uroot -pvecentek@vetdb ethernetdb_pub";
        String vetFileName = "\\vet\\wctt_vet_" + format + ".sql";

        log.info("开始备份数据库");
        System.out.println("开始备份数据库");

        executeBackupTask(mcntChmod, mcntFileName, "蜂窝");
        executeBackupTask(btChmod, btFileName, "蓝牙");
        executeBackupTask(comalsChmod, comalsFileName, "通信");
        executeBackupTask(v2xChmod, v2xFileName, "v2x");
        executeBackupTask(platformChmod, platformFileName, "平台");
        executeBackupTask(wifiChmod, wifiFileName, "wifi");
        executeBackupTask(rfChmod, rfFileName, "射频");
        executeBackupTask(gnssChmod, gnssFileName, "gnss");
        executeBackupTask(vstChmod, vstFileName, "漏扫");
        executeBackupTask(binaryChmod, binaryFileName, "二进制");
        executeBackupTask(dstChmod, dstFileName, "数据安全");
        executeBackupTask(appChmod, appFileName, "app");
        executeBackupTask(vcanChmod, vcanFileName, "总线");
        executeBackupTask(vetChmod, vetFileName, "以太网");
    }

    public void executeBackupTask(String command, String fileName, String dataBaseName) {
        MysqldumpConstant.EXECUTOR.submit(() -> {
            try {
                Process process = Runtime.getRuntime().exec(command);
                // 处理标准输出流
                putProcessInputStream2File(process.getInputStream(), fileName);
                // 处理标准错误流
                putProcessInputStream2File(process.getErrorStream(), null);
                // 等待命令执行完毕
                int exitCode = process.waitFor();
                if (exitCode == 0) {
                    System.out.println(dataBaseName + "：数据库备份成功！");
                    log.info(dataBaseName + "：数据库备份成功！");
                } else {
                    System.out.println(dataBaseName + "：数据库备份失败！");
                    log.info(dataBaseName + "：数据库备份失败！");
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }


    public void putProcessInputStream2File(InputStream inputStream, String fileName) throws IOException {
        OutputStream outputStream;
        if (fileName == null) {
            // 输出错误日志
            outputStream = new FileOutputStream(MysqldumpConstant.logFilePath, true);
        } else {
            // 输出sql文件
            outputStream = new FileOutputStream(MysqldumpConstant.mysqlFilePath + fileName);
        }
        // 设置缓冲区大小（可选）
        byte[] buff = new byte[1024];
        int size;
        // 从输入流读取数据并写入输出流
        while ((size = inputStream.read(buff)) > 0) {
            outputStream.write(buff, 0, size);
        }
        inputStream.close();
        outputStream.close();
    }

}
