package com.example.mysqldumptest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MysqldumptestApplication {

    public static void main(String[] args) {
        SpringApplication.run(MysqldumptestApplication.class, args);
    }

}
