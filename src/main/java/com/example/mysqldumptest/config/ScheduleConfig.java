package com.example.mysqldumptest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * @Author YangLi
 * @Date 2024/1/19 14:53
 * @注释
 */
public class ScheduleConfig {

    /**
     * 搞一个线程数为10的线程池
     * @return  返回定时任务执行器
     */
    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(10);
        return taskScheduler;
    }

}
